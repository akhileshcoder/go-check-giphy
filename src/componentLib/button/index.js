import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Loader from '../loader';
import Helpers from '../utils/helpers';

/**
 * Button component class to create a traditional html button
 * or href or clickable span based on the props.
 */
export default class Button extends React.Component {
	constructor (props) {
		super(props);
		this.state = {
			ready: false
		};
		this.elem = null;
		this.setUpRef = this.setUpRef.bind(this);
	}

	componentWillMount () {
		setTimeout(() => {
			this.setState({ready: true});
		}, 0);
	}

	getButtonClassNames (receivedClass) {
		const { color, disabled, fullWidth, link, loading,
			outline, rounded, size } = this.props;
		let buttonClass = cx('btn', 'bdr-transparent justify-content-ce btn',
			{'i-flex': !Helpers.isBrowserIE11()});
		if (disabled || loading) {
			buttonClass = cx(buttonClass, {
				'btn-rounded': rounded,
				'bdr-rad4': !rounded,
				'btn-disabled': loading
			});
		} else if (outline || link) {
			buttonClass = cx(buttonClass, `bg-white ${color}`,
				[`btn${color[0].toUpperCase() + color.substr(1).toLowerCase()}Outline`],
				{
					'btn-rounded': rounded,
					'bdr-rad4': !rounded,
					[`bdr-${color}`]: outline
				});
		} else {
			buttonClass = cx(buttonClass, 'white', `bg-${color}`,
				[`btn${color[0].toUpperCase() + color.substr(1).toLowerCase()}`],
				{
					'btn-rounded': rounded,
					'bdr-rad4': !rounded
				});
		}

		if (size === 'large') {
			buttonClass = cx(buttonClass, 'ft-sz-16');
		} else if (size === 'small') {
			buttonClass = cx(buttonClass, 'ft-sz-12');
		} else {
			buttonClass = cx(buttonClass, 'ft-sz-14');
		}
		buttonClass = cx(buttonClass, {
			[`btn-block flex justify-content-ce width100`]: fullWidth
		});

		if (fullWidth) {
			buttonClass = cx(buttonClass, 'btn-block');
		}
		return cx(buttonClass, receivedClass);
	}

	setUpRef (e) {
		this.elem = e;
	}

	render () {
		const { color, disabled, loading, link, outline, rounded,
			fullWidth, className, href, style, size, ...otherProps
		} = this.props;
		let buttonClass = this.getButtonClassNames(className),
			s = {};
		let modifiedStyle = Object.assign({}, style);

		// Decide whether an anchor tag or a button must be created.
		const Tag = href
			? 'a'
			: 'button';
		if (loading) {
			if (!this.state.ready) {
				return (
					<Tag
						className={buttonClass}
						href={href}
						{...otherProps}
						ref={this.setUpRef}
						style={{ visibility: 'hidden' }}
					>
						{this.props.children || 'A'}
					</Tag>
				);
			}
			/**
			 * Cannot be tested as it is dom actions.
			 */
			if (this.elem) {
				s.width = this.elem.getBoundingClientRect().width;
				s.height = this.elem.getBoundingClientRect().height;
				s.padding = '0';
				modifiedStyle = Object.assign(modifiedStyle, s);
			}
			return (
				Helpers.isBrowserIE11()
					? (<div className="i-flex">
						<Tag className={buttonClass} ref={this.setUpRef} style={modifiedStyle} {...otherProps}>
							<Loader />
						</Tag>
					</div>)
					: (<Tag className={buttonClass} ref={this.setUpRef} style={modifiedStyle} {...otherProps}>
						<Loader />
					</Tag>)
			);
		} else if (disabled) {
			return (
				<Tag className={buttonClass} disabled style={modifiedStyle} {...otherProps}>
					{this.props.children}
				</Tag>
			);
		} else {
			return (
				<Tag
					className={buttonClass}
					href={href}
					ref={this.setUpRef}
					style={modifiedStyle}
					{...otherProps}
				>
					{this.props.children}
				</Tag>
			);
		}
	}
}

Button.propTypes = {
	/**
	 * All standard colors (e.g. blue, green, mint, yellow, red etc)
	 */
	color: PropTypes.string,
	/**
	 * @ignore
	 * optional css class string
	 */
	className: PropTypes.string,
	/**
	 * optional children node(s)/component(s)
	 * @ignore
	 */
	children: PropTypes.node,
	/**
	 * Sets the attribute disabled to the button
	 */
	disabled: PropTypes.bool,
	/**
	 * Block button that expands over 100% of the parent
	 */
	fullWidth: PropTypes.bool,
	/**
	 * Hyperhref for changing button to render as anchor
	 */
	href: PropTypes.string,
	/**
	 * Similar to outline button without outline
	 */
	link: PropTypes.bool,
	/**
	 * Activates the loading animation and sets the button to disabled
	 */
	loading: PropTypes.bool,
	/**
	 * Makes the button outlined with white background
	 */
	outline: PropTypes.bool,
	/**
	 * Makes the button rounded
	 */
	rounded: PropTypes.bool,
	/**
	 * Size of the button
	 */
	size: PropTypes.oneOf(['small', 'regular', 'large']),
	/**
	 * Additional styling applied to the button
	 * @ignore
	 */
	style: PropTypes.object // eslint-disable-line react/forbid-prop-types
};

Button.defaultProps = {
	children: null,
	className: '',
	color: 'gray',
	disabled: false,
	fullWidth: false,
	link: false,
	loading: false,
	outline: false,
	rounded: false,
	href: null,
	size: 'regular',
	style: null
};
