import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

const COMMIT_TIMEOUT = 500;
/**
 * Input field allow users to enter single line text.
 * Input text supports all html5 behaviour and attributes depending upon browser support of html5.
 */
export default class Input extends Component {
  constructor(props) {
    super(props);
    this.elem = null;
    this.setUpRef = this.setUpRef.bind(this);
    this.state = {
      value: this.props.value,
      message: "",
      isFocused: false,
      isHovering: false
    };
  }
  static contextTypes = {
    disabledInputs: PropTypes.bool
  };

  static get NORMAL() {
    return "normal";
  }

  static get UNDERLINE() {
    return "underline";
  }
  setUpRef(e) {
    this.elem = e.target;
  }
  componentWillReceiveProps({ value }) {
    // Prevents any new prop changes from changing whats currently in the field in case
    // the user is active on it and has a pending change.
    if (!this.__pendingCommit) {
      this.setState({ value });
    }
  }

  componentWillUnmount() {
    if (this._focusTimer) {
      this.__pendingCommit = false;
      clearTimeout(this._focusTimer);
    }
  }
  componentDidMount() {
    if (this.props.focus) {
      //Timeout added to override the react select focus after 50ms.
      this._focusTimer = setTimeout(() => {
        this.textInput.focus();
      }, 51);
    }
  }
  render() {
    const {
      type,
      className,
      inputStyle,
      onChange,
      value,
      label,
      ...otherProps
    } = this.props;
    const inputClass = cx(
      [`body-small pad-05r`],
      {
        "bdr-rad4 def-inp": inputStyle === Input.NORMAL,
        "bottom-bdr": inputStyle === Input.UNDERLINE
      },
      className
    );

    const inp = (
      <input
        type={type}
        onChange={this.handleOnChange}
        value={this.state.value}
        {...otherProps}
        className={inputClass}
      />
    );

    return label ? (
        <div className="label-parent">
          <label>{label}</label>
          {inp}
        </div>
    ) : inp
  }
  handleOnChange = e => {
    let value = e.target.value;
    this.setState({
      value
    });
    clearTimeout(this.__commitTimer);
    this.__pendingCommit = true;
    this.__commitTimer = setTimeout(() => this.onCommit(), COMMIT_TIMEOUT);
  };
  onCommit = () => {
    const { value } = this.state;
    this.__pendingCommit = false;
    clearTimeout(this.__commitTimer);
    if (value !== this.props.value) {
      this.props.onChange(value);
    }
  };
}
