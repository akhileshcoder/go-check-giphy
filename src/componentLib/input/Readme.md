   ### Default Input
     
       <Input type="text" placeholder="Default Input" />
   
   ### Bottom border style Input     
      <Input type="text" placeholder="Bottom border style Input" inputStyle={Input.UNDERLINE} size="50"/>
   
  ### Number type input       
          <Input type="number" placeholder="Number Type Input" inputStyle={Input.UNDERLINE}/>
   
   ### Full width input
       <div className="flex flex-col">
            <Input type="text" placeholder="Full Width Default Input" className="flex1"/>
            <Input type="text" placeholder="Full Width Bottom Border Style Input" inputStyle={Input.UNDERLINE} className="flex1" />
        </div>
        
   ### Input with on change event.
        initialState = {name:''};
        const handleOnChange = function(event){
             setState({name: event.target.value});
        };
        
        <div>
            <Input type="text" placeholder="Enter your name" onChange={handleOnChange} value={state.name}/> 
            {state.name && <Badge color='blue' className="mar-left-1r">Hello {state.name}</Badge>}
        </div>
              
