import React, {PureComponent} from 'react';
import cns from 'classnames';

export default class Card extends PureComponent {
	render() {
		const {
			title,
			body,
			footer,
			isBorderLess,
			className,
			titleClassName,
			bodyClassName,
		} = this.props;
		return (
			<div className={cns('card-wrapper', className, { 'card-border': !isBorderLess })}>
				{title &&
				(typeof title === 'string' ? (
					<div className={cns('card-li', 'card-title', titleClassName)}>{title}</div>
				) : (
					<div className={cns('card-li', 'card-title')}> {title} </div>
				))}
				{body &&
				(typeof body === 'string' ? (
					<div className={cns('card-li', 'card-body', bodyClassName)}> {body} </div>
				) : (
					<div className={cns('card-li', 'card-body')}> {body} </div>
				))}
				{footer &&
				(typeof footer === 'string' ? (
					<div className={cns('card-li', 'card-foot')}> {footer} </div>
				) : (
					<div className={cns('card-li', 'card-foot')}> {footer} </div>
				))}
			</div>
		);
	}
}
