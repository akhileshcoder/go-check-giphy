import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

/**
 * Generic component that can be used to insert a component to the body of the application.
 * Use with caution as this may polltu the DOM.
 * PropTypes validation not required as it is an internal class.
 */
class RenderInBody extends React.Component {
	componentDidMount () {
		const { id } = this.props;
		this.container = document.createElement('div');
		this.container.setAttribute('id', id);
		document.body.insertBefore(this.container, document.body.firstChild);
		this.appendToContainer();
	}

	componentDidUpdate () {
		this.appendToContainer();
	}

	componentWillUnmount () {
		setTimeout(() => {
			ReactDOM.unmountComponentAtNode(this.container);
			document.body.removeChild(this.container);
		}, this.props.delayBeforeUnmount);
	}

	appendToContainer () {
		ReactDOM.render(this.props.children, this.container);
	}

	render () {
		// Render a placeholder
		return null;
	}
}
RenderInBody.propTypes = {
	/**
	 * children comonent(s)
	 */
	children: PropTypes.node,
	/**
	 * wait time in ms before unmounting the component
	 */
	delayBeforeUnmount: PropTypes.number,
	id: PropTypes.string
};

RenderInBody.defaultProps = {
	children: null,
	delayBeforeUnmount: 0,
	id: 'rib_' + Date.now()
};

export default RenderInBody;
