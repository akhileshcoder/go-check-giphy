/* eslint no-unused-expressions: 0 */

import {SillySearch} from "./SillySearch";
const chai = require('chai'),
    expect = chai.expect,
    _ = require('underscore');
 const silly = new SillySearch();
describe('silly', function(){
    describe('.match', function(){
        it('should return the rendered string and match score', function(){
            const results = SillySearch.match('ab', 'ZaZbZ');
            results && expect(results.rendered).to.equal('Z<b>a</b>Z<b>b</b>Z');
            expect(results).to.include.keys('score');
        });
        it('should return null on no match', function(){
            expect(SillySearch.match('ZEBRA!', 'ZaZbZ')).to.equal(null);
        });
        it('should return a greater score for consecutive matches of pattern', function(){
            const search = SillySearch.match('abcd', 'zabcd');
            const search1 = SillySearch.match('abcd', 'azbcd');
            const consecutiveScore = search && search.score;
            const scatteredScore = search1 && search1.score;
            expect(consecutiveScore).to.be.above(scatteredScore);
        });
    });
    describe('.filter', function(){
        it('should return list untouched when the pattern is undefined', function() {
            const arr = ['aba', 'c', 'cacb'];
            const result = silly.filter(undefined, arr);
            expect(result).to.equal(arr);
        });
        it('should return an empty array when the array is undefined', function() {
            const result = silly.filter('pattern', undefined);
            expect(result).to.deep.equal([]);
        });
        it('should return an empty array when the array is empty', function() {
            const result = silly.filter('pattern', []);
            expect(result).to.deep.equal([]);
        });
        it('should return the index and matching array elements', function(){
            const result = silly.filter('ab', ['aba', 'c', 'cacb']);
            expect(result).to.have.length(2);
            expect(result[0].index).to.equal(0);
            expect(result[0]).to.have.property('score');
            expect(result[1].index).to.equal(2);
            expect(result[1]).to.have.property('score');
        });
        it('should return list untouched when given empty pattern', function(){
            const arr = 'abcdefghjklmnop'.split('');
            const results = _.pluck(silly.filter('', arr), 'string');
            expect(results).to.eql(arr);
        });
    });
});
