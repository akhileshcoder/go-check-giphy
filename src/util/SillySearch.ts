export class SillySearch {

    static match(pattern, str) {
        let patternIdx = 0,
            result: any = [],
            len = str.length,
            totalScore = 0,
            currScore = 0,
            pre = '<b>',
            post = '</b>',
            compareString = str.toLowerCase(),
            ch;

        pattern = pattern.toLowerCase();
        for (let idx = 0; idx < len; idx++) {
            ch = str[idx];
            if (compareString[idx] === pattern[patternIdx]) {
                ch = pre + ch + post;
                patternIdx += 1;
                currScore += 1 + currScore;
            } else {
                currScore = 0;
            }
            totalScore += currScore;
            result[result.length] = ch;
        }


        if (patternIdx === pattern.length) {
            totalScore = (compareString === pattern) ? Infinity : totalScore;
            return {rendered: result.join(''), score: totalScore};
        }

        return null;
    };

    filter(pattern, arr) {
        if (!arr || arr.length === 0) {
            return [];
        }
        if (typeof pattern !== 'string') {
            return arr;
        }
        return arr
            .reduce((prev, element, idx) => {
                let rendered = SillySearch.match(pattern, element);
                if (rendered != null) {
                    prev[prev.length] = {
                        string: rendered.rendered,
                        score: rendered.score,
                        index: idx,
                        original: element
                    };
                }
                return prev;
            }, [])
            .sort(function (a, b) {
                let compare = b.score - a.score;
                if (compare) return compare;
                return a.index - b.index;
            });
    };
}
