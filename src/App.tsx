import React, {Component, Fragment} from "react";

import Card from "./componentLib/card";
import Input from "./componentLib/input";

import {ASSET_TYPE, AVAILABLE_KEYS, ASSET_PATH, getDynamicKey} from "./smiley/smiley";
import {SmileyService} from "./util/smiley.service";
import {getFromGiphy} from "./util/gifApiUtil";
import {SmileyGroup} from "./custom/SmileyHeader";

class App extends Component {
    state;
    smiley = new SmileyService();

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            searchKey: "",
            smileyArrText: "",
            viewType: ASSET_TYPE.TEXT_EMOJI
        };
        this.searchSmiley('', this.state.viewType, true,
            smileyArrText => Object.assign(this.state, {smileyArrText}));
    }

    render() {
        const {text, searchKey, smileyArrText, viewType} = this.state;
        // @ts-ignore
        return (
            <div className="app-root">
                <Card
                    title={
                        <Fragment>
                            <div>Go Check</div>
                            <div className="text-container" dangerouslySetInnerHTML={{__html: text}}/>
                        </Fragment>
                    }
                    body={
                        <Fragment>
                            <div className="flex mar-bottom-05r">
                                <Input
                                    label="Search Smiley"
                                    type="text"
                                    placeholder="Enter any silly thing you know "
                                    onChange={value => this.searchSmiley("searchKey", value)}
                                    value={searchKey}
                                    inputStyle={Input.UNDERLINE}
                                    className="flex1"
                                />
                            </div>
                            <div className="flex mar-bottom-05r btn-group">
                                <SmileyGroup viewType={viewType} searchSmiley={this.searchSmiley}/>
                            </div>
                        </Fragment>
                    }
                    footer={
                        <div className="smiley-selector" onClick={e => this.addSmiley(e)}
                             dangerouslySetInnerHTML={{__html: smileyArrText}}>
                        </div>
                    }
                    isBorderLess={true}
                />
            </div>
        );
    }

    addSmiley(e) {
        let {viewType, text} = this.state;
        if (!AVAILABLE_KEYS.valid.includes(viewType)) {
            if (e.target.classList.contains('handler')) {
                let elm = e.target.closest('.clickable-gif-parent');
                if (elm) {
                    const rand = Math.floor(Math.random() * 1000 * 1000);
                    let innerHtm = elm.innerHTML;
                    innerHtm = innerHtm.split('"original_still_')
                        .join(`"original_${rand}_still_`)
                        .split('float: left;').join('')
                        .split('<div class="handler">+</div>').join('');
                    text += innerHtm;
                    this.setState({text});
                }
            }
        } else {
            let elm = e.target;
            let attr = elm && elm.getAttribute('data-click');
            attr || (elm = e.target.closest('[data-click]'));
            attr = attr || (elm && elm.getAttribute('data-click'));
            attr = attr && JSON.parse(attr);
            if (attr) {
                text = App.__getSmileyHtml(attr, text, viewType).split(/float\s*:\s*left;*/g).join('');
                this.setState({text});
            }
        }
    }

    static __getSmileyHtml(i, txt, viewType) {
        let str = JSON.stringify(i);
        if (AVAILABLE_KEYS.text.includes(viewType)) {
            txt += `
                        <span class="btn" data-click='${str}' data-title="${i.k}">${i.v}</span>
                    `;
        } else if (
            AVAILABLE_KEYS.static.includes(viewType)
        ) {
            txt += `
                        <span class="btn" data-click='${str}' data-title="${i.k}">
                            <img src="${ASSET_PATH[viewType].p}${i.k}.png" />
                        </span>
                    `;
        } else if (
            AVAILABLE_KEYS.dynamic.includes(viewType)
        ) {
            let dirN;
            switch (viewType) {
                case ASSET_TYPE.DYNAMIC_CUTE:
                    dirN = "cute";
                    break;
                case ASSET_TYPE.DYNAMIC_MINION:
                    dirN = "minion";
                    break;
                default:
                    break
            }
            txt += `<span class="${getDynamicKey(dirN, i.k)}" data-click='${str}' style="float: left" data-title="${i.k}"></span>`;
        }
        return txt;
    }

    searchSmiley = (key, value, noStateUpdate?: Boolean, callback?: Function) => {
        const cb = () => {
            const {searchKey, viewType} = this.state;
            if (AVAILABLE_KEYS.valid.includes(viewType)) {
                let smileyArrText = '';
                const ser = this.smiley.search(searchKey, viewType);
                ser.forEach(i => {
                    smileyArrText = App.__getSmileyHtml(i, smileyArrText, viewType)
                });
                noStateUpdate || this.setState({smileyArrText});
                callback && callback(smileyArrText);
            } else {
                getFromGiphy(searchKey, smileyArrText => {
                    noStateUpdate || this.setState({smileyArrText});
                    callback && callback(smileyArrText);
                })
            }
        };
        noStateUpdate
            ? cb()
            : this.setState({[key]: value}, cb);
    };
}

export default App;
