var express = require("express"),
    path = require("path"),
    app = express(),
 bodyParser = require("body-parser"),
    cors = require("cors");

app.use(cors());
app.use(bodyParser.json({ limit: "5mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "5mb" }));
app.use(express.static(path.resolve(__dirname, "./build")));

var srvr = app.listen(3333, function() {
    console.log("listening on port : ", 3333);
});
